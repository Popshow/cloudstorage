import Vue from 'vue'
import router from "@/router";
import store from "@/store";
import { sync } from 'vuex-router-sync'
import App from "@/App";
import './index.css'

sync(store, router)

new Vue({
  el: '#app',
  store,
  router,
  render: h => h(App),
})