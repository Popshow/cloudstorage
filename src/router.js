import Vue from 'vue'
import Router from 'vue-router'
import Cloud from "@/pages/Cloud";
import Auth from "@/pages/Auth";

Vue.use(Router);

const router = new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            component: Auth,
            name: 'Auth',
        },
        {
            path: '/cloud',
            component: Cloud,
            name: 'Cloud',
            meta: {requiresAuth: true}
        },
        {
            path: '*',
            component: 404,
            name: '404',
        }
    ]
})

router.beforeEach((to, from, next) => {
    const requiresAuth = to.matched.some( record => record.meta.requiresAuth );
    const login = localStorage.getItem('auth_token');

    if (requiresAuth && login) return next();
    if (requiresAuth && !login) return next({name: 'Auth'});
    if (!requiresAuth && login) return next({name: 'Cloud'});
    if (to.name === "Auth" && login) return next({name: 'Cloud'});

    next();
})



export default router
