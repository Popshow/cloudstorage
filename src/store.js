import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";
import { URL_API } from "@/variable";
import router from "@/router";
import { getAxios } from "./utils/requestApi";

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    auth: {
      isLogin: false,
      token: "",
      errorLogin: "",
      errorPassword: "",
    },
    files: [],
  },
  mutations: {
    refreshAuthStateLogin(state) {
      if (
        state.auth.isLogin === false &&
        localStorage.getItem("auth_token") !== null
      ) {
        state.auth.token = localStorage.getItem("auth_token");
        state.auth.isLogin = true;
      }
    },

    changeAuthStateLogin(state, data) {
      if (data.status === 400) {
        state.auth.errorLogin = data.data.login.toString();
        state.auth.errorPassword = data.data.password.toString();
      } else {
        state.auth.token = data.data;
        state.auth.isLogin = true;
        localStorage.setItem("auth_token", data.data);
        state.auth.errorLogin = state.auth.errorPassword = "";
        router.push({ name: "Cloud" }).then((res) => res);
      }
    },
    changeAuthStateLogout(state, data) {
      if (data.status === 200) {
        state.auth.isLogin = false;
        state.auth.token = data.data;
        localStorage.removeItem("auth_token");
        router.push({ name: "Auth" }).then((res) => res);
      }
    },
    getAllFilesState(state, data) {
      if (data.status === 200) {
        state.files = data.data;
      }
    },
    postAddFile(state, data) {
      if (data.status === 200) {
        state.files = data.data;
      }
    },
  },
  actions: {
    sendLogin(ctx, data) {
      axios
        .post(URL_API + "/login", data)
        .then((res) => {
          ctx.commit("changeAuthStateLogin", res);
        })
        .catch((error) => {
          ctx.commit("changeAuthStateLogin", error.response);
        });
    },
    sendLogout(ctx, data) {
      axios
        .post(URL_API + "/logout", data, {
          headers: {
            "auth-token": `Bearer ${this.state.auth.token}`,
          },
        })
        .then((res) => {
          ctx.commit("changeAuthStateLogout", res);
        })
        .catch((error) => {
          ctx.commit("changeAuthStateLogout", error.response);
        });
    },
    getAllFilesAction(ctx) {
      getAxios("/list")
        .then((res) => {
          ctx.commit("getAllFilesState", res);
        })
        .catch((error) => {
          ctx.commit("getAllFilesState", error.response);
        });
    },
    postAddFileAction(ctx, data) {
      axios
        .post(URL_API + `/file?filename=${data.name}`, data.form, {
          headers: {
            "auth-token": `Bearer ${this.state.auth.token}`,
          },
        })
        .then((res) => {
          ctx.commit("postAddFile", res);
        })
        .catch((error) => {
          ctx.commit("postAddFile", error.response);
        });
    },
    deleteFileAction(ctx, data) {
      axios
        .delete(URL_API + `/file?filename=${data}`, {
          headers: {
            "auth-token": `Bearer ${this.state.auth.token}`,
          },
        })
        .then(() => {
          this.dispatch("getAllFilesAction");
        });
    },

    downloadFileAction(ctx, name) {
      axios
        .get(URL_API + `/file?filename=${name}`, {
          responseType: "blob",
          headers: {
            "content-type": "charset=utf-8",
            "auth-token": `Bearer ${this.state.auth.token}`,
          },
        })
        .then((response) => {
          const url = window.URL.createObjectURL(new Blob([response.data]));
          const link = document.createElement("a");
          link.href = url;
          link.setAttribute("download", response.config.url.split("=")[1]);
          document.body.appendChild(link);
          link.click();
        });
    },
  },
  getters: {
    userIsLogin(state) {
      return state.auth;
    },
    allFiles(state) {
      return state.files;
    },
  },
});

export default store;
