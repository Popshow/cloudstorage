import axios from "axios";
import { URL_API } from "@/variable";

const headers = {
  "auth-token": `Bearer ${localStorage.getItem("auth_token")}`,
};

export const getAxios = async (direction = "") => {
  try {
    return await axios.get(URL_API + direction, { headers });
  } catch (error) {
    console.log(error);
  }
};
